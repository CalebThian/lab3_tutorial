#include <iostream>
#include <fstream>
#include <cstdlib>
#include <ctime>
#include <cmath>
using namespace std;

int main(){
	
	ofstream f;
	f.open("file.in",ios::out);
	if(!f){
		cerr<<"Failed opening file.in"<<endl;
		exit(1);
	}

	int h=1000; 
	int num=rand()%9995+5;
	f<<num<<endl;

	for(int i=0;i<num;++i)
		f<<rand()%1000+100<<endl;
	
	f.close();
	return 0;
}
