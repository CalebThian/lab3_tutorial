#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>
using namespace std;


bool myfunction(int i,int j){ return i>j;};

int main(){
	ifstream f;
	f.open("file.in",ios::in);
	if(!f){
		cerr<<"Failed opening file.in"<<endl;
		exit(1);
	}
	
	int num;
	f>>num;

	vector <int> h_list;
	int temp;
	for(int i=0;i<num;++i){
		f>>temp;
		h_list.push_back(temp);
	}
	
	f.close();

	sort(h_list.begin(),h_list.end(),myfunction);

	int sum=0;
	for(int i=0;i<5;++i)
		sum+=h_list.at(i);
	cout<<sum<<endl;

	return 0;
}
